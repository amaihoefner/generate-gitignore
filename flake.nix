{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    naersk.url = "github:nix-community/naersk/master";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    naersk,
  }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
      };
      naersk-lib = pkgs.callPackage naersk {};
    in {
      defaultPackage = naersk-lib.buildPackage {
        src = ./.;

        buildInputs = with pkgs; [
          pkg-config
          openssl
        ];
      };
      devShell = with pkgs;
        mkShell {
          buildInputs = [
            cargo
            rustc
            rustfmt
            clippy
            rust-analyzer

            pre-commit

            pkg-config
            openssl
          ];

          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
    });
}
