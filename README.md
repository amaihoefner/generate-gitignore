Interactively query [gitignore.io](https://www.toptal.com/developers/gitignore) and write output to stdout or file.

See `generate-gitignore --help` for usage.
