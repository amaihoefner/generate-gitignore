use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::{collections::HashMap, io::stdout};

use anyhow::anyhow;
use serde::{Deserialize, Serialize};
use skim::prelude::*;
use structopt::StructOpt;

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord)]
#[serde(rename_all = "camelCase")]
struct GitignoreTemplate {
    key: String,
    name: String,
    file_name: String,
    contents: String,
}

impl GitignoreTemplate {
    async fn get_templates() -> anyhow::Result<Vec<Self>> {
        let templates_map: HashMap<String, GitignoreTemplate> =
            reqwest::get("https://www.toptal.com/developers/gitignore/api/list?format=json")
                .await?
                .json()
                .await?;

        let mut templates: Vec<GitignoreTemplate> = templates_map.into_values().collect();
        templates.sort();

        Ok(templates)
    }
}

impl SkimItem for GitignoreTemplate {
    fn text(&self) -> Cow<str> {
        Cow::Borrowed(&self.name)
    }

    fn preview(&self, _context: PreviewContext) -> ItemPreview {
        ItemPreview::Text(self.contents.trim().to_string())
    }

    fn output(&self) -> Cow<str> {
        Cow::Borrowed(self.contents.trim())
    }
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "generate-gitignore",
    about = "Query and select gitignore templates from gitignore.io."
)]
struct Opt {
    /// Output file, stdout if not present
    #[structopt(long, short, parse(from_os_str))]
    output: Option<PathBuf>,

    /// Overwrite output file if it exists
    #[structopt(long, short)]
    force: bool,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    let mut output: Box<dyn Write> = if let Some(output_path) = opt.output {
        if output_path.exists() && !opt.force {
            return Err(anyhow!(
                "{output_path:?} already exists. Use '--force' to overwrite."
            ));
        }

        let output_file = File::create(output_path)?;

        Box::new(output_file)
    } else {
        Box::new(stdout())
    };

    let templates: Vec<GitignoreTemplate> = GitignoreTemplate::get_templates().await?;

    let skim_options = SkimOptionsBuilder::default()
        .prompt(Some("Select gitignore templates: "))
        .multi(true)
        .preview(Some(""))
        .build()
        .unwrap();

    let (tx_item, rx_item): (SkimItemSender, SkimItemReceiver) = unbounded();
    for template in templates {
        let _ = tx_item.send(Arc::new(template));
    }
    drop(tx_item);

    let selected_items = Skim::run_with(&skim_options, Some(rx_item))
        .map(|out| out.selected_items)
        .unwrap_or_default();

    let gitignore_contents = selected_items.iter().map(|item| item.output());

    for content in gitignore_contents {
        writeln!(output, "{}\n", content)?;
    }

    Ok(())
}
